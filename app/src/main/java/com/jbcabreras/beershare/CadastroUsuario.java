package com.jbcabreras.beershare;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jbcabreras.beershare.dominio.RepositorioUsuario;
import com.jbcabreras.beershare.dominio.entidades.Sessao;

public class CadastroUsuario extends AppCompatActivity {

    private RepositorioUsuario repositorioUsuario;

    private EditText campoUsuario;
    private EditText campoTelefone;
    private EditText campoCerveja;
    private EditText campoEmail;
    private EditText campoSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_usuario);

        campoUsuario = (EditText) findViewById(R.id.cadUsuario);
        campoTelefone = (EditText) findViewById(R.id.cadTelefone);
        campoCerveja = (EditText) findViewById(R.id.cadCerveja);
        campoEmail = (EditText) findViewById(R.id.cadEmail);
        campoSenha = (EditText) findViewById(R.id.cadSenha);

        try {

            repositorioUsuario = new RepositorioUsuario(getApplicationContext());

        }catch(SQLException ex){

            Toast.makeText(this, "Erro no Banco de Dados", Toast.LENGTH_SHORT).show();
        }

    }

    public void cadastrar(View view){

        String usuario = campoUsuario.getText().toString();
        String telefone = campoTelefone.getText().toString();
        String cerveja = campoCerveja.getText().toString();
        String email = campoEmail.getText().toString();
        String senha = campoSenha.getText().toString();

        if(usuario != null && telefone != null && email != null && senha != null){

            try {

                repositorioUsuario.inserir(usuario, telefone, cerveja, email, senha);


                Toast.makeText(this, "Usuario cadastrado com SUCESSO", Toast.LENGTH_SHORT).show();

                Sessao.setSessao(repositorioUsuario.buscaUsuario(email, senha).get(0));

                Intent it = new Intent(this, MainActivity.class);
                startActivity(it);

            }catch (SQLException e){

                Toast.makeText(this, "Erro no Banco de Dados", Toast.LENGTH_SHORT).show();
            }

        }else{

            Toast.makeText(this, "Favor completar os campos", Toast.LENGTH_SHORT).show();
        }
    }
}
