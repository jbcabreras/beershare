package com.jbcabreras.beershare.dominio.entidades;

import java.io.Serializable;

public class Cerveja implements Serializable{

    public static String TABELA = "CERVEJA";
    public static String ID = "_id";
    public static String URL = "URL";
    public static String TIPO = "TIPO";
    public static String NOME = "NOME";
    public static String CARACTERISTICA = "CARACTERISTICA";
    public static String IDUSUARIO = "IDUSUARIO";

    private int id;
    private String url;
    private String tipo;
    private String nome;
    private String caracteristica;
    private String idUsuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString()
    {
        return id+url+tipo+nome+caracteristica+idUsuario;
    }


}
