package com.jbcabreras.beershare.dominio.entidades;

import java.io.Serializable;

public class Usuario implements Serializable{

    public static String TABELA = "USUARIO";
    public static String ID = "_id";
    public static String USUARIO = "USUARIO";
    public static String TELEFONE = "TELEFONE";
    public static String CERVEJA = "CERVEJA";
    public static String EMAIL = "EMAIL";
    public static String SENHA = "SENHA";

    private int id;
    private String usuario;
    private String telefone;
    private String cerveja;
    private String email;
    private String senha;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCerveja() {
        return cerveja;
    }

    public void setCerveja(String cerveja) {
        this.cerveja = cerveja;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString()
    {
        return usuario + " " + telefone;
    }


}
