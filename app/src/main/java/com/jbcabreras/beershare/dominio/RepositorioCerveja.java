package com.jbcabreras.beershare.dominio;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jbcabreras.beershare.database.DataBase;
import com.jbcabreras.beershare.dominio.entidades.Cerveja;

import java.util.ArrayList;


public class RepositorioCerveja {

    private DataBase dataBase;
    private SQLiteDatabase conn;

    public RepositorioCerveja(Context context){

        dataBase = new DataBase(context);
        this.conn = dataBase.getWritableDatabase();
    }

    public void inserir(String url, String tipo, String nome, String caracteristica, int idusuario){

        String insert = "INSERT INTO CERVEJA(URL, TIPO, NOME, CARACTERISTICA, IDUSUARIO) VALUES('"
                + url + "','"
                + tipo + "','"
                + nome + "','"
                + caracteristica + "','"
                + idusuario + "')";

        conn.execSQL(insert);
    }

    public ArrayList<Cerveja> buscaTodasCervejas()
    {
        ArrayList<Cerveja> lista = new ArrayList<>();

        String select = "SELECT * FROM CERVEJA";

        Cursor cursor  =  conn.rawQuery(select, null);

        if (cursor.getCount() > 0 )
        {

            cursor.moveToFirst();

            do {

                Cerveja cerveja = new Cerveja();
                cerveja.setId(cursor.getInt(cursor.getColumnIndex(Cerveja.ID)));
                cerveja.setUrl(cursor.getString(cursor.getColumnIndex(Cerveja.URL)));
                cerveja.setTipo(cursor.getString(cursor.getColumnIndex(Cerveja.TIPO)));
                cerveja.setNome(cursor.getString(cursor.getColumnIndex(Cerveja.NOME)));
                cerveja.setCaracteristica(cursor.getString(cursor.getColumnIndex(Cerveja.CARACTERISTICA)));
                cerveja.setIdUsuario(cursor.getString(cursor.getColumnIndex(Cerveja.IDUSUARIO)));

                lista.add(cerveja);

            }while (cursor.moveToNext());

        }

        return lista;

    }

    public ArrayList<Cerveja> buscaCervejasUsuario(int id)
    {
        ArrayList<Cerveja> lista = new ArrayList<>();

        String select = "SELECT * FROM CERVEJA WHERE IDUSUARIO = "+ id;

        Cursor cursor  =  conn.rawQuery(select, null);

        if (cursor.getCount() > 0 )
        {

            cursor.moveToFirst();

            do {

                Cerveja cerveja = new Cerveja();
                cerveja.setId(cursor.getInt(cursor.getColumnIndex(Cerveja.ID)));
                cerveja.setUrl(cursor.getString(cursor.getColumnIndex(Cerveja.URL)));
                cerveja.setTipo(cursor.getString(cursor.getColumnIndex(Cerveja.TIPO)));
                cerveja.setNome(cursor.getString(cursor.getColumnIndex(Cerveja.NOME)));
                cerveja.setCaracteristica(cursor.getString(cursor.getColumnIndex(Cerveja.CARACTERISTICA)));
                cerveja.setIdUsuario(cursor.getString(cursor.getColumnIndex(Cerveja.IDUSUARIO)));

                lista.add(cerveja);

            }while (cursor.moveToNext());

        }

        return lista;

    }

}
