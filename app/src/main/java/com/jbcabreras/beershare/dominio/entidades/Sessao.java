package com.jbcabreras.beershare.dominio.entidades;

/**
 * Created by rodri on 19/11/2015.
 */
public class Sessao {

    private static int idUsuario;
    private static String usuario;
    private static String telefone;
    private static String cerveja;
    private static String email;

    public Sessao(Usuario usuario){
        idUsuario = usuario.getId();
        Sessao.usuario = usuario.getUsuario().toString();
        telefone = usuario.getTelefone().toString();
        cerveja = usuario.getCerveja().toString();
        email = usuario.getEmail().toString();
    }

    public static void setSessao(Usuario usuario){
        idUsuario = usuario.getId();
        Sessao.usuario = usuario.getUsuario().toString();
        telefone = usuario.getTelefone().toString();
        cerveja = usuario.getCerveja().toString();
        email = usuario.getEmail().toString();
    }

    public static int getIdUsuario() {
        return idUsuario;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static String getTelefone() {
        return telefone;
    }

    public static String getCerveja() {
        return cerveja;
    }

    public static String getEmail() {
        return email;
    }

    public static String getSessao(){
        return idUsuario+usuario+telefone+cerveja+email;
    }
}


