package com.jbcabreras.beershare.dominio;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jbcabreras.beershare.database.DataBase;
import com.jbcabreras.beershare.dominio.entidades.Usuario;

import java.util.ArrayList;


public class RepositorioUsuario {

    private DataBase dataBase;
    private SQLiteDatabase conn;

    public RepositorioUsuario(Context context){

        dataBase = new DataBase(context);
        this.conn = dataBase.getWritableDatabase();
    }

    public void inserir(String usuario, String telefone, String cerveja, String email, String senha){

        String insert = "INSERT INTO USUARIO(USUARIO, TELEFONE, CERVEJA, EMAIL, SENHA) VALUES('"
                + usuario + "','"
                + telefone + "','"
                + cerveja + "','"
                + email + "','"
                + senha + "')";

        conn.execSQL(insert);
    }

    public ArrayList<Usuario> buscaUsuario(String email, String senha)
    {
        ArrayList<Usuario> lista = new ArrayList<>();

        String select = "SELECT * FROM USUARIO WHERE EMAIL = '"
                + email
                + "' AND SENHA = '"
                + senha +"'";

        Cursor cursor  =  conn.rawQuery(select, null);

        if (cursor.getCount() > 0 )
        {

            cursor.moveToFirst();

            do {

                Usuario usuario = new Usuario();
                usuario.setId(cursor.getInt(cursor.getColumnIndex(Usuario.ID)));
                usuario.setUsuario(cursor.getString(cursor.getColumnIndex(Usuario.USUARIO)));
                usuario.setTelefone(cursor.getString(cursor.getColumnIndex(Usuario.TELEFONE)));
                usuario.setCerveja(cursor.getString(cursor.getColumnIndex(Usuario.CERVEJA)));
                usuario.setEmail(cursor.getString(cursor.getColumnIndex(Usuario.EMAIL)));
                usuario.setSenha(cursor.getString(cursor.getColumnIndex(Usuario.SENHA)));

                lista.add(usuario);

            }while (cursor.moveToNext());

        }

        return lista;

    }

}
