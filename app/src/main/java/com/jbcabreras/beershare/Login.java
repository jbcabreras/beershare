package com.jbcabreras.beershare;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jbcabreras.beershare.dominio.RepositorioUsuario;
import com.jbcabreras.beershare.dominio.entidades.Sessao;
import com.jbcabreras.beershare.dominio.entidades.Usuario;

import java.util.ArrayList;

public class Login extends AppCompatActivity {

   private RepositorioUsuario repositorioUsuario;
    private ArrayList loginResult;

    private EditText campoEmail;
    private EditText campoSenha;
    private Button btnLogin;

    public static Sessao sessao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        campoEmail = (EditText) findViewById(R.id.campoEmail);
        campoSenha = (EditText) findViewById(R.id.campoSenha);
        btnLogin = (Button) findViewById(R.id.botaoLogin);

        try{

            repositorioUsuario = new RepositorioUsuario(getApplicationContext());

        }catch (SQLException e){

            Toast.makeText(this, "Erro ao criar o banco de dados", Toast.LENGTH_SHORT).show();

        }
    }

    public void login(View view){

        try{

            String email = campoEmail.getText().toString();
            String senha = campoSenha.getText().toString();

            loginResult = repositorioUsuario.buscaUsuario(email, senha);

            if(loginResult.size() > 0){

                sessao = new Sessao((Usuario) loginResult.get(0));

                Intent it = new Intent(Login.this, MainActivity.class);
                startActivity(it);

            }else{

                Toast.makeText(this, "Usuario ou senha inválidos", Toast.LENGTH_SHORT).show();

            }

        }catch (SQLException e){

            Toast.makeText(this, "Erro no Banco de Dados", Toast.LENGTH_SHORT).show();

        }

    }

    public void cadastrar(View view){

        Intent it = new Intent(this, CadastroUsuario.class);
        startActivity(it);

    }

     /*public void recuperar(View view){
        Intent it = new Intent(Login.this, CadastroUsuario.class);
        startActivity(it);
    }*/
}
