package com.jbcabreras.beershare.database;

/**
 * Created by rodri on 18/11/2015.
 */
public class ScriptSQL {

    public static String getCreateUsuario(){
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(" CREATE TABLE IF NOT EXISTS USUARIO ( ");
        sqlBuilder.append("_id                INTEGER NOT NULL ");
        sqlBuilder.append("PRIMARY KEY AUTOINCREMENT, ");
        sqlBuilder.append("USUARIO            VARCHAR (200) NOT NULL, ");
        sqlBuilder.append("TELEFONE           VARCHAR (14)  NOT NULL, ");
        sqlBuilder.append("CERVEJA            VARCHAR (255), ");
        sqlBuilder.append("EMAIL              VARCHAR (255) NOT NULL, ");
        sqlBuilder.append("SENHA              VARCHAR (10) NOT NULL)");

        return sqlBuilder.toString();
    }

    public static String getCreateCerveja(){
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(" CREATE TABLE IF NOT EXISTS CERVEJA ( ");
        sqlBuilder.append("_id             INTEGER NOT NULL ");
        sqlBuilder.append("PRIMARY KEY AUTOINCREMENT, ");
        sqlBuilder.append("URL             VARCHAR (255) NOT NULL, ");
        sqlBuilder.append("TIPO            VARCHAR (30)  NOT NULL, ");
        sqlBuilder.append("NOME            VARCHAR (30), ");
        sqlBuilder.append("CARACTERISTICA  VARCHAR (500), ");
        sqlBuilder.append("IDUSUARIO       INTEGER NOT NULL, ");
        sqlBuilder.append("CONSTRAINT Cerveja_Usuario_fk FOREIGN KEY (IDUSUARIO) REFERENCES Usuario (IDUSUARIO))");

        return sqlBuilder.toString();
    }
}
