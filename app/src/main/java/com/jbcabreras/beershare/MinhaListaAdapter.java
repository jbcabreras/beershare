package com.jbcabreras.beershare;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jbcabreras.beershare.dominio.entidades.Cerveja;

import java.util.ArrayList;

/**
 * Created by rodri on 24/11/2015.
 */
public class MinhaListaAdapter extends ArrayAdapter<Cerveja>
{
    private final Context context;
    private ArrayList<Cerveja> minhaLista;

    public MinhaListaAdapter(Context context, ArrayList<Cerveja> lista)
    {
        super(context, R.layout.main, lista);
        this.context = context;
        minhaLista = lista;
    }

    @Override
    public View getView(int iPosicao, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vLinha = inflater.inflate(R.layout.minha_lista_item, parent, false);

        String texto = "Estilo: "+minhaLista.get(iPosicao).getTipo()+"\n"+
                       "Nome: "+minhaLista.get(iPosicao).getNome()+"\n"+
                       "Características: "+minhaLista.get(iPosicao).getCaracteristica()+"\n";

        TextView mlTexto = (TextView) vLinha.findViewById(R.id.mlTexto);
        mlTexto.setText(texto);

        if(!minhaLista.get(iPosicao).getUrl().equals("")){

            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 4;

            final Bitmap bitmap = BitmapFactory.decodeFile(minhaLista.get(iPosicao).getUrl(),
                    options);

            ImageView foto = (ImageView) vLinha.findViewById(R.id.mlImagem);

            foto.setImageBitmap(bitmap);

        }

        return vLinha;
    }


    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

}
