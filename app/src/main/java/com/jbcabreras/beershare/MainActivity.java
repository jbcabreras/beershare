package com.jbcabreras.beershare;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.jbcabreras.beershare.dominio.RepositorioCerveja;
import com.jbcabreras.beershare.dominio.entidades.Sessao;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
{
    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int PIC_CROP = 2;

    // directory name to store captured images and videos
    private static final String IMAGE_DIRECTORY_NAME = "BeerShare";
    private static String caminhoFoto;

    private Uri fileUri; // file url to store image/video
    private Uri picUri;
    private Bitmap thePic;
    private MinhaListaAdapter minhaListaAdapter;
    private ListView lvLista;
    private ListView lvListaHome;

    TabHost tabHost;

    private RepositorioCerveja repositorioCerveja;

    private ImageView imgCamera;
    private ImageView imgPreview;
    private Spinner spEstilos;
    private EditText campoNomeCerveja;
    private EditText campoCaract;
    private Button botaoAdicionar;



    @Override
    public void onCreate(Bundle savedInstanceState)
     {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        tabHost=(TabHost)findViewById(R.id.tabHost);
        tabHost.setup();
      
        TabSpec spec1=tabHost.newTabSpec(getResources().getString(R.string.tab_lista));
        spec1.setContent(R.id.tab1);
        spec1.setIndicator(getResources().getString(R.string.tab_lista));

        TabSpec spec2=tabHost.newTabSpec(getResources().getString(R.string.tab_todas));
        spec2.setIndicator(getResources().getString(R.string.tab_home));
        spec2.setContent(R.id.tab2);
            
        TabSpec spec3=tabHost.newTabSpec(getResources().getString(R.string.tab_adicionar));
        spec3.setContent(R.id.tab3);
        spec3.setIndicator(getResources().getString(R.string.tab_adicionar));

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

         Spinner spEstilo = (Spinner) findViewById(R.id.spEstilos);

         CharSequence[] strings = getResources().getTextArray(R.array.lista_estilos);
         ArrayAdapter<CharSequence> estiloAdapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_dropdown_item, strings){
             @Override
             public View getView(int position, View convertView, ViewGroup parent) {
                 View view =super.getView(position, convertView, parent);
                 TextView textView=(TextView) view.findViewById(android.R.id.text1);
                 // do whatever you want with this text view
                 textView.setTextSize(20);
                 textView.setTextColor(Color.DKGRAY);
                 return view;
             }
         };

         estiloAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         spEstilo.setAdapter(estiloAdapter);

         imgPreview = (ImageView) findViewById(R.id.imgPreview);
         imgCamera = (ImageView) findViewById(R.id.botaoCamera);
         spEstilos = (Spinner) findViewById(R.id.spEstilos);
         campoNomeCerveja = (EditText) findViewById(R.id.campoNomeCerveja);
         campoCaract = (EditText) findViewById(R.id.campoCaract);

         try {

             repositorioCerveja = new RepositorioCerveja(getApplicationContext());

         }catch(SQLException ex){

             Toast.makeText(this, "Erro no Banco de Dados", Toast.LENGTH_SHORT).show();
         }

         lvLista = (ListView) findViewById(R.id.tab1LisView);
         lvLista.setAdapter(new MinhaListaAdapter(getApplicationContext(), repositorioCerveja.buscaCervejasUsuario(Sessao.getIdUsuario())));

         lvListaHome = (ListView) findViewById(R.id.tab2LisView);
         lvListaHome.setAdapter(new MinhaListaAdapter(getApplicationContext(), repositorioCerveja.buscaTodasCervejas()));

         Log.v("banco", repositorioCerveja.buscaCervejasUsuario(Sessao.getIdUsuario()).toString());
         Log.v("sessao", Sessao.getSessao());

		/*
		 * Capture image button click event
		 */
         imgCamera.setOnClickListener(new View.OnClickListener() {

             @Override
             public void onClick(View v) {
                 // capture picture
                 captureImage();
             }
         });

         // Checking camera availability
         if (!isDeviceSupportCamera()) {
             Toast.makeText(getApplicationContext(),
                     "Seu dispositivo nao suporta câmera",
                     Toast.LENGTH_LONG).show();
             // will close the app if the device does't have camera
             finish();
         }
     }

    /**
     * Checking device has camera hardware or not
     * */
    private boolean isDeviceSupportCamera() {
        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /*
     * Capturing Camera Image will lauch camera app requrest image capture
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /*
     * Here we store the file url as it will be null after returning from camera
     * app
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }


    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                //previewCapturedImage();
                performCrop();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "Captura cancelada", Toast.LENGTH_SHORT)
                        .show();
                //user is returning from cropping the image
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Falha ao capturar imagem", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == PIC_CROP) {
            //get the returned data
            Bundle extras = data.getExtras();
            //get the cropped bitmap
            thePic = extras.getParcelable("data");
            previewCapturedImage();
        }
    }

    private void performCrop(){

        try {

            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 680);
            cropIntent.putExtra("outputY", 680);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);

        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Seu dispositivo nao suporta CROP";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /*
     * Display image from a path to ImageView
     */
    private void previewCapturedImage() {
        try {

            imgCamera.setVisibility(View.GONE);
            imgPreview.setVisibility(View.VISIBLE);
            imgPreview.setImageBitmap(thePic);

//            // bimatp factory
//            BitmapFactory.Options options = new BitmapFactory.Options();
//
//            // downsizing image as it throws OutOfMemory Exception for larger
//            // images
//            options.inSampleSize = 4;
//
//            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
//                    options);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * ------------ Helper Methods ----------------------
     * */

	/*
	 * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /*
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Falha ao criar "
                        + IMAGE_DIRECTORY_NAME + " diretório");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");

            caminhoFoto = mediaFile.getPath().toString();
            //Log.v("url", caminhoFoto);

        } else {
            return null;
        }

        return mediaFile;
    }

    public void Adicionar(View view){

        String url = caminhoFoto;
        String tipo = spEstilos.getSelectedItem().toString();
        String nome = campoNomeCerveja.getText().toString();
        String caracteristica = campoCaract.getText().toString();

        if(url != null && tipo != null){

            try {

                repositorioCerveja.inserir(url, tipo, nome, caracteristica, Sessao.getIdUsuario());

                Toast.makeText(this, "Cerveja cadastrada com SUCESSO", Toast.LENGTH_SHORT).show();

                Intent it = new Intent(this, MainActivity.class);
                startActivity(it);

            }catch (SQLException e){

                Toast.makeText(this, "Erro no Banco de Dados - Adicionar", Toast.LENGTH_SHORT).show();
            }

        }else{

            Toast.makeText(this, "Favor completar os campos", Toast.LENGTH_SHORT).show();
        }
    }
}
